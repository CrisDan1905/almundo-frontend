import { Injectable } from '@angular/core';
import { CRUDService } from './crud.service';
import { Hotel } from '../interfaces/hotel.interface';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HotelsService {

  // testHotel = {
  //     "id": "161901",
  //     "name": "Hotel Santa Cruz",
  //     stars: 3,
  //     starsArray: [1,1,1],
  //     "price": 1267.57,
  //     "image": "6623490_6_b.jpg",
  //     "amenities": [
  //       "nightclub",
  //       "business-center",
  //       "bathtub",
  //       "newspaper",
  //       "restaurant"
  //     ]
  //   ,
  // }

  constructor(private CRUD: CRUDService) { }

  hotels: Hotel[] = [];
  hotelsLoaded: Boolean = false;

  getHotels(params?: object) {
    this.hotelsLoaded = false;

    this.CRUD.get('hotels', params)
      .pipe(map(({data}) => {
        data.map(val => val.starsArray = Array.from({length: val.stars}));
        return data;
      }))
      .subscribe(res => {
        this.hotelsLoaded = true;
        this.hotels = res;
      });
  }
}
