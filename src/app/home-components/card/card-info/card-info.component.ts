import { Component, OnInit, Input } from '@angular/core';
import { HotelsService } from '../../../services/hotels.service';
import { Hotel } from '../../../interfaces/hotel.interface';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-card-info',
  templateUrl: './card-info.component.html',
  styleUrls: ['./card-info.component.css']
})
export class CardInfoComponent implements OnInit {

  @Input() hotel: Hotel;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
  }

  get sanitizedImg() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(`data:${this.hotel.image.contentType};base64, ${this.hotel.image.data}`);
  }

}
