import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardpriceComponent } from './card-price.component';

describe('CardpriceComponent', () => {
  let component: CardpriceComponent;
  let fixture: ComponentFixture<CardpriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardpriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardpriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
