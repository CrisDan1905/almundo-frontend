import { Component, OnInit, Input } from '@angular/core';
import { HotelsService } from '../../../services/hotels.service';
import { Hotel } from '../../../interfaces/hotel.interface';

@Component({
  selector: 'app-card-price',
  templateUrl: './card-price.component.html',
  styleUrls: ['./card-price.component.css']
})
export class CardpriceComponent implements OnInit {

  @Input() hotel: Hotel;

  constructor() { }

  ngOnInit() {
  }

}
