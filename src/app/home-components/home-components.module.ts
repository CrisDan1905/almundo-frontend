import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FilterHeaderComponent } from './filters/filter-header/filter-header.component';
import { FilterComponent } from './filters/filter.component';
import { FilterHotelComponent } from './filters/filter-hotel/filter-hotel.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FilterStarsComponent } from './filters/filter-stars/filter-stars.component';
import { ToggleMenuDirective } from './filters/directives/toggle-menu.directive';
import { CardComponent } from './card/card.component';
import { CardInfoComponent } from './card/card-info/card-info.component';
import { CardpriceComponent } from './card/card-price/card-price.component';
import { LoadingStatusComponent } from './loading-status/loading-status.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [
    HeaderComponent,
    FilterHeaderComponent,
    FilterComponent,
    FilterHotelComponent,
    FilterStarsComponent,
    ToggleMenuDirective,
    CardComponent,
    CardInfoComponent,
    CardpriceComponent,
    LoadingStatusComponent
  ],
  exports: [
    HeaderComponent,
    FilterComponent,
    CardComponent,
    LoadingStatusComponent

  ]
})
export class HomeComponentsModule { }
