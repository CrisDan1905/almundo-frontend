import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FilterComponent implements OnInit {

  filtersForm: ReactiveFormsModule;

  @ViewChild('filterBody') filterBody: ElementRef;

  constructor(private renderer: Renderer2, private fb: FormBuilder) { }

  ngOnInit() {
    this.filtersForm = this.fb.group({
      name: ['', Validators.required],
      stars: this.fb.array(Array.from({length: 5}, i => this.fb.control(false)))
    });
  }

  toggleFilters() {
    if (this.filterBody.nativeElement.classList.contains('filter__body--hidden'))
      this.renderer.removeClass(this.filterBody.nativeElement, 'filter__body--hidden')
    else
      this.renderer.addClass(this.filterBody.nativeElement, 'filter__body--hidden')
  }

}
