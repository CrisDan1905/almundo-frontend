import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HotelsService } from '../../../services/hotels.service';

@Component({
  selector: 'app-filter-hotel',
  templateUrl: './filter-hotel.component.html',
  styleUrls: ['./filter-hotel.component.css']
})
export class FilterHotelComponent implements OnInit {

  @ViewChild('hotelNameMenu') hotelNameMenu: ElementRef;

  searchForm: Object;

  private searched: Boolean = false;

  @Input() filtersForm: FormGroup;

  constructor(private hotels: HotelsService) { }

  ngOnInit() {
    this.filtersForm.get('name').valueChanges.subscribe(val => {
      if (!val && this.searched) {
        this.hotels.getHotels();
        this.searched = false;
      }
    });

  }

  private toggleMenu() {
    this.hotelNameMenu.nativeElement.class.toggle();
  }

  private getStarsQuery(arrayToQuery) {
    return arrayToQuery
      .map((val, i) => val ? i + 1 : 0)
      .filter((val, i) => val ? i + 1 : 0);
  }

  filterHotels() {
    const params = {
      name: this.filtersForm.value.name,
      stars: this.getStarsQuery(this.filtersForm.value.stars)
    };
    this.hotels.getHotels(params);
    this.searched = true;
  }



}
