import { Directive, HostListener, ElementRef, Renderer2, ContentChild } from '@angular/core';

@Directive({
    selector: '[appToggleMenu]'
})
export class ToggleMenuDirective {

  @ContentChild('arrow') arrowEl: ElementRef;

    constructor(private el: ElementRef, private renderer: Renderer2) { }



    @HostListener('click') toggleMenu() {
        const parent = this.el.nativeElement.parentNode;

        if (parent.classList.contains('filter__section--close'))
            this.renderer.removeClass(parent, 'filter__section--close');
        else
        this.renderer.addClass(parent, 'filter__section--close');

        if (this.arrowEl.nativeElement.classList.contains('filter__expander--open'))
          this.renderer.removeClass(this.arrowEl.nativeElement, 'filter__expander--open');
        else
          this.renderer.addClass(this.arrowEl.nativeElement, 'filter__expander--open');

    }

}
