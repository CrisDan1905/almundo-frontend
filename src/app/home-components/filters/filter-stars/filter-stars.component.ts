import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'app-filter-stars',
  templateUrl: './filter-stars.component.html',
  styleUrls: ['./filter-stars.component.css']
})
export class FilterStarsComponent implements OnInit {

  @Input() filtersForm: FormGroup;

  get starsFormArray(): FormArray {
    return this.filtersForm.get('stars') as FormArray;
  }

  getStarsAmount(count) {
    return Array.from({length: count});
  }

  constructor() { }

  ngOnInit() { }


  selectAllStars(checked) {
    this.starsFormArray.controls.map(control => control.setValue(checked));
  }
}
