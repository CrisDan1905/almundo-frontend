import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponentsModule } from './home-components/home-components.module';
import localeAr from '@angular/common/locales/es-AR';
import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

registerLocaleData(localeAr, 'ar');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HomeComponentsModule,
    HttpClientModule,
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'de-at' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
