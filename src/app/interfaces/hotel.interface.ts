export interface Hotel {
  id: number;
  name: string;
  stars: number;
  starsArray: any[];
  price: number;
  image: {data: string, contentType: string};
  amenities: string[];
}
