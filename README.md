#  Sobre la aplicación

* Esta es una aplicación creada con angular CLI 6.0.3
* Fueron utilizados formularios reactivos para la realización de los filtros. A futuro se podrian utilizar para realizar las validaciones necesarias
* Se implementó una interfaz para determinar los campos que debe contener cada registro de hotel
* Se muestra una pantalla de cargando mientras se cargan los hoteles en cada consulta
* Se realizaron cada uno de los componentes necesarios en una carpeta llamada `home-components`. Allí se encuentran estructurados de la siguiente manera:

  +-- card: Seccion referente a las tarjetas donde se visualizarán los usuarios.

    +-- card-info: Sección de imagenes, estrellas y comodidades de cada hotel.
    +-- card-price: Sección referente al precio de cada hotel.

  +-- filters: Sección dividida en tres partes que representa cada sección de los filtros.  

    +-- filter-header: Cabecera de los filtors.  
    +-- filter-hotel: Donde se escribe nombre del hotel y se puede iniciar la busqueda.  
    +-- filter-stars: Donde se selecciona el filtro por estrellas de los hoteles.  

  +-- header: Cabecera de la página.  

  +-- loading-status: Plantilla que se muestra mientras se cargan los hoteles.  

* Existen dos archivos para el manejo de variables de entorno. Se usará uno o el otro automaticamente dependiendo del entorno en el que se elija correr la aplicación.

  +-- environment.prod.ts: Variables de entorno de producción.  
  +-- environment.ts: Variables de entorno de desarrollo.  

# Antes de iniciar la aplicación
Cuando se vaya a iniciar la aplicación por primera vez se debe usar el comando `npm install`. Este instalará todas las dependencias necesarias para que el proyecto funcione

# Cómo iniciar la aplicación en entorno de desarrollo
**NOTA: El backend debe estar corriendo tambien en entorno de desarrollo**

Correr el comando `ng serve`. 

# Cómo iniciar la aplicación en entorno de producción
**NOTA: El backend debe estar corriendo tambien en entorno de producción**

Existen dos formas:
 1. Corriendo el comando `ng serve --prod`. Esto correrá la aplicacion simulando un estado de producción y tomará las variables de entorno definidas como producción. Sin embargo no debe usarse propiamente en producción, pues no tiene todas las optimizaciones que este necesita.
 
 2. Se corre el comando `ng build --prod` y se monta todo lo que esté dentro de la carpeta `dist/` en el servidor web donde se desee poner a funcionar la aplicación
